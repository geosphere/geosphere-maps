'use client';

import dynamic from 'next/dynamic';
import initI18n from './i18n';

const Map = dynamic(() => import('./map'), { ssr: false });

export default function Home({ params: { locale } }: { params: { locale: string } }) {
  initI18n(locale);
  return <Map locale={locale}></Map>;
}

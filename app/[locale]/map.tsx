'use client';

import { useRef, useState, useEffect } from 'react';
import { Root, createRoot } from 'react-dom/client';

import dynamic from 'next/dynamic';

import { useTranslation } from 'react-i18next';

import MapView from '@arcgis/core/views/MapView';
import WebMap from '@arcgis/core/WebMap';
import esriConfig from '@arcgis/core/config';
import ScaleBar from '@arcgis/core/widgets/ScaleBar';
import Legend from '@arcgis/core/widgets/Legend';
import * as intl from '@arcgis/core/intl';
import TextContent from '@arcgis/core/popup/content/TextContent';
import ExpressionContent from '@arcgis/core/popup/content/ExpressionContent';
import ElementExpressionInfo from '@arcgis/core/popup/ElementExpressionInfo';
import { watch } from '@arcgis/core/core/reactiveUtils';
// @ts-ignore
import { getColorsForRendererValues } from '@arcgis/core/renderers/support/utils';

// set asset path for ArcGIS Maps SDK widgets
esriConfig.assetsPath = '/assets';

// ids of web map items in portal
const webMapDEID = '7d0768f73d3e4be2b32c22274c600cb3';
const webMapENID = 'dbf5532d06954c6a989d4f022de83f70';

// lazy load components
const Layers = dynamic(() => import('./layer-list'));
const Print = dynamic(() => import('./print'));
const Basemaps = dynamic(() => import('./basemap-list'));
const Search = dynamic(() => import('./search'));

// import Calcite components
import '@esri/calcite-components/dist/calcite/calcite.css';

import { setAssetPath } from '@esri/calcite-components/dist/components';
setAssetPath(window.location.href);

import '@esri/calcite-components/dist/components/calcite-shell';
import '@esri/calcite-components/dist/components/calcite-shell-panel';
import '@esri/calcite-components/dist/components/calcite-shell-center-row';
import '@esri/calcite-components/dist/components/calcite-action-bar';
import '@esri/calcite-components/dist/components/calcite-action';
import '@esri/calcite-components/dist/components/calcite-panel';
import {
  CalciteShell,
  CalciteShellPanel,
  CalciteActionBar,
  CalciteAction,
  CalcitePanel,
} from '@esri/calcite-components-react';
import MapImageLayer from '@arcgis/core/layers/MapImageLayer';

export default function MapComponent({ locale }: { locale: string }) {
  const maskRef = useRef<HTMLDivElement>(null);
  const tableRef = useRef<HTMLDivElement>(null);
  const mapView = useRef<MapView | null>(null);
  const previousId = useRef<string | null>(null);
  const mapRef = useRef<HTMLDivElement | null>(null);
  const layersRef = useRef<HTMLDivElement>(null);
  const layersRoot = useRef<Root | null>(null);
  const legendRef = useRef<HTMLDivElement>(null);
  const basemapsRef = useRef<HTMLDivElement>(null);
  const basemapsRoot = useRef<Root | null>(null);
  const printRef = useRef<HTMLDivElement>(null);
  const printRoot = useRef<Root | null>(null);
  const searchRef = useRef<HTMLDivElement>(document.createElement('div'));
  const searchRoot = useRef<Root | null>(null);

  const [actionBarExpanded, setActionBarExpanded] = useState<boolean>(false);

  const { t } = useTranslation();

  useEffect(() => {
    if (mapRef.current) {
      // set locale for ArcGIS Maps SDK widgets
      intl.setLocale(locale);

      const webMap = new WebMap({
        portalItem: {
          id: locale === 'de' ? webMapDEID : webMapENID,
          portal: {
            url: 'https://gis.geosphere.at/portal',
          },
        },
      });

      const view = new MapView({
        container: mapRef.current,
        map: webMap,
        padding: {
          left: 49,
        },
        popup: {
          dockOptions: {
            position: 'top-right',
            breakpoint: false,
          },
          dockEnabled: true,
        },
        extent: {
          ymax: 6424330,
          xmin: 923200,
          xmax: 2017806,
          ymin: 5616270,
          spatialReference: {
            wkid: 3857,
          },
        },
        ui: {
          components: ['attribution'],
        },
        popupEnabled: true,
      });

      mapView.current = view;

      // add ScaleBar
      const scaleBar = new ScaleBar({
        view: view,
        unit: 'metric',
      });

      view.ui.add([scaleBar], 'bottom-left');

      // render Legend component
      if (legendRef.current) {
        new Legend({
          view: view,
          container: legendRef.current,
        });
      }

      // render Search component
      if (searchRef.current) {
        if (!searchRoot.current) {
          searchRoot.current = createRoot(searchRef.current);
        }

        searchRoot.current.render(<Search view={view}></Search>);
      }

      // render Layers component
      if (layersRef.current) {
        if (!layersRoot.current) {
          layersRoot.current = createRoot(layersRef.current);
        }

        if (tableRef.current) {
          layersRoot.current.render(<Layers view={view} tableDiv={tableRef.current}></Layers>);
        }
      }

      // render Basemaps component
      if (basemapsRef.current) {
        if (!basemapsRoot.current) {
          basemapsRoot.current = createRoot(basemapsRef.current);
        }

        basemapsRoot.current.render(<Basemaps view={mapView.current}></Basemaps>);
      }

      // // render Print component
      if (printRef.current) {
        if (!printRoot.current) {
          printRoot.current = createRoot(printRef.current);
        }

        if (maskRef.current) {
          printRoot.current.render(<Print view={mapView.current} maskDiv={maskRef.current}></Print>);
        }
      }

      watch(
        () => view.popup?.viewModel?.active,
        () => console.log(view.popup?.selectedFeature)
      );

      // view.on('immediate-click', (event) => {
      //   const mapImageLayer = view.map.layers.find((layer) => layer.title === 'Profilschnitte') as MapImageLayer;
      //   if (mapImageLayer) {
      //     mapImageLayer.allSublayers.forEach((sublayer) => {
      //       if (sublayer.title === 'Bohrprofile') {
      //       }
      //     });
      //   }
      // });

      // This function fires each time a LayerView is created
      // view.on('layerview-create', function (event) {
      //   // The LayerView for the desired layer
      //   if (event.layer.type === 'map-image') {
      //     (event.layer as MapImageLayer).allSublayers.forEach((sublayer) => {
      //       if (sublayer.title === 'Bohrprofile') {
      //         let textElement = new TextContent();
      //         textElement.text = 'Das ist nur ein Test.';
      //         if (Array.isArray(sublayer.popupTemplate.content)) sublayer.popupTemplate.content.push(textElement);

      //         sublayer.load().then(async () => {
      //           const renderer = sublayer.renderer;
      //           const fieldToValueColorMap = await getColorsForRendererValues(renderer);

      //           let classBreaks: number[] = [];
      //           let colors: object[] = [];
      //           for (const [key, classBreaksToColorMap] of fieldToValueColorMap) {
      //             for (const [classBreack, color] of classBreaksToColorMap) {
      //               colors.push(color);
      //               classBreaks.push(classBreack);
      //             }

      //             //   while (!stopIteration) {
      //             //     if ($feature['${key}'] > ${classBreaks}[index]) {
      //             //       stopIteration = true;
      //             //       color = ${colors}[index]
      //             //     }
      //             // }

      //             const expressionContent = new ExpressionContent({
      //               expressionInfo: {
      //                 title: 'Legende',
      //                 expression: `
      //                 var stopIteration = true;
      //                 var index = 0;

      //                 return {
      //                   type: "text",
      //                   text: "First class break for key ${key}: ${classBreaks[0]} and value: " + $feature['${key}']
      //                 }
      //               `,
      //               },
      //             });

      //             if (Array.isArray(sublayer.popupTemplate.content)) {
      //               sublayer.popupTemplate.content.push(expressionContent);
      //             }
      //           }
      //         });
      //       }
      //     });
      //   }
      // });
    }
  }, [locale, t]);

  const handleCalciteActionBarToggle = () => {
    setActionBarExpanded(!actionBarExpanded);
    if (mapView.current) {
      mapView.current.padding = !actionBarExpanded ? { left: 150 } : { left: 49 };
    }

    if (tableRef.current) {
      if (!actionBarExpanded) {
        tableRef.current.classList.add('left-40');
        tableRef.current.classList.remove('left-14');
      } else {
        tableRef.current.classList.add('left-14');
        tableRef.current.classList.remove('left-40');
      }
    }
  };

  const handleClick = (event: any) => {
    if (event.target.tagName !== 'CALCITE-ACTION') {
      return;
    }

    const nextId = event.target.dataset.actionId;

    if (previousId.current) {
      const previousPanel = document.querySelector(`[data-panel-id=${previousId.current}]`) as HTMLCalcitePanelElement;
      if (previousPanel) {
        previousPanel.hidden = true;
      }

      if (previousId.current === 'print') {
        maskRef.current?.classList.add('hidden');
      }
    }

    const nextPanel = document.querySelector(`[data-panel-id=${nextId}]`) as HTMLCalcitePanelElement;
    if (nextPanel && nextId !== previousId.current) {
      nextPanel.hidden = false;
      previousId.current = nextId;

      if (nextId === 'print') {
        maskRef.current?.classList.remove('hidden');
      }
    } else {
      previousId.current = null;
    }
  };

  return (
    <div>
      <CalciteShell contentBehind>
        <CalciteShellPanel slot="panel-start" displayMode="float">
          <CalciteActionBar
            slot="action-bar"
            onCalciteActionBarToggle={handleCalciteActionBarToggle}
            className="border-r border-r-gray-400"
          >
            <CalciteAction
              data-action-id="layers"
              icon="layers"
              text={t('layers.title')}
              onClick={handleClick}
            ></CalciteAction>
            <CalciteAction
              data-action-id="basemaps"
              icon="basemap"
              text={t('basemaps.title')}
              onClick={handleClick}
            ></CalciteAction>
            <CalciteAction
              data-action-id="legend"
              icon="legend"
              text={t('legend.title')}
              onClick={handleClick}
            ></CalciteAction>
            <CalciteAction
              data-action-id="print"
              icon="print"
              text={t('print.heading')}
              onClick={handleClick}
            ></CalciteAction>
            <CalciteAction data-action-id="info" icon="information" text="Info" onClick={handleClick}></CalciteAction>
          </CalciteActionBar>

          <CalcitePanel data-panel-id="layers" heading={t('layers.title')} hidden>
            <div ref={layersRef}></div>
          </CalcitePanel>

          <CalcitePanel data-panel-id="basemaps" heading={t('basemaps.title')} hidden>
            <div ref={basemapsRef}></div>
          </CalcitePanel>

          <CalcitePanel data-panel-id="legend" heading={t('legend.title')} hidden>
            <div ref={legendRef}></div>
          </CalcitePanel>

          <CalcitePanel data-panel-id="print" heading={t('print.heading')} hidden>
            <div ref={printRef}></div>
          </CalcitePanel>

          <CalcitePanel data-panel-id="info" heading="Info" hidden>
            <div id="info-container"></div>
          </CalcitePanel>
        </CalciteShellPanel>
        <div className="h-screen w-full" ref={mapRef}>
          <div ref={tableRef} className="hidden absolute left-14 bottom-5 h-1/3 right-2 border border-gray-400"></div>
        </div>
        <div
          ref={maskRef}
          className="hidden absolute bg-red-300 border-2 border-red-600 opacity-50 pointer-events-none"
        ></div>
      </CalciteShell>
    </div>
  );
}

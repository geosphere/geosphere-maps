import '../globals.css';

import type { Metadata } from 'next';
import { Source_Sans_3 } from 'next/font/google';

const ss3 = Source_Sans_3({
  subsets: ['latin'],
  display: 'swap',
});

export const metadata: Metadata = {
  title: 'GeoSphere Maps',
  description: 'GeoSphere Maps',
  icons: { icon: '/assets/favicon.ico' },
};

export default function RootLayout({ children, params }: { children: React.ReactNode; params: any }) {
  return (
    <html lang={params.locale} className={ss3.className}>
      <body className="w-full h-full p-0 m-0">{children}</body>
    </html>
  );
}

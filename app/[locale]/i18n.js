import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import deJSON from './dictionaries/de.json';
import enJSON from './dictionaries/en.json';

// define translations
const resources = {
  en: {
    translation: enJSON,
  },
  de: {
    translation: deJSON,
  },
};

// initialize i18n with given locale
const initI18n = (locale) =>
  i18n.use(initReactI18next).init({
    resources,
    lng: locale,
    interpolation: {
      escapeValue: false,
    },
  });

export default initI18n;
